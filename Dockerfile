ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

COPY docker-entrypoint docker-requirements.txt /

RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y gcc \
&& pip install -r /docker-requirements.txt \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
;

ENTRYPOINT ["/docker-entrypoint"]

